<?php

echo "<style>
body {background-color: powderblue;}
h1   {color: blue;}
p    {color: red;}
</style>";


echo"Deklarasikan dan menampilkan array<br>";
$arrbuah=array("Harimau","Singa","Macan","Kuda");
echo $arrbuah[0]."<br>";
echo $arrbuah[3]."<br><br>";

$arrwarna=array();
$arrwarna[]="Merah";
$arrwarna[]="Putih";
$arrwarna[]="Hijau";
$arrwarna[]="Hitam";

echo $arrwarna[0]."<br>";
echo $arrwarna[2 ]."<br>";
echo "----------------------------------------------------------------"."<br>"."<br>";

// asosiatif

echo"array asosiatif<br>";

$arrmedali=array("Akhyar"=>"emas","lukas"=>"silver","nicholas"=>"perak");
echo $arrmedali['Akhyar']."<br>";
echo $arrmedali['lukas']."<br>";
echo $arrmedali['nicholas']."<br><br>";

$arraywarna1=array();
$arraywarna1['sukirman']="Merah";
$arraywarna1["subaedah"]="Putih";
$arraywarna1["sumenep"]="Hijau";
$arraywarna1["acep"]="Hitam";

echo $arraywarna1['subaedah']."<br>";
echo $arraywarna1['acep']."<br>";
echo "----------------------------------------------------------------"."<br>"."<br>";

// for dan foreach
echo"For dan Foreach<br>";
$arrbendera=array("Red","White");

echo"Menampilkan isi array dengan FOR: <br>";
for($i=0;$i<count($arrbendera);$i++){
    echo "Warna Bendera Indonesia<font color=$arrbendera[$i]>".$arrbendera[$i]."</font><br>";

}
echo"<br>";
echo"Menampilkan isi array dengan FOREACH: <br>";
foreach($arrbendera as$warna){
    echo "Warna Bendera Indonesia<font color=$warna>".$warna."</font><br>";

}

echo "----------------------------------------------------------------"."<br>"."<br>";
// foreach dan while-list
echo"Foreach<br>";
$arrmedal=array("Akhyar"=>"emas","lukas"=>"silver","nicholas"=>"perak");
echo "Menmpilkan isi array asosiatif dengan foreach: <br>";
echo "Perolehan medali<br>";
foreach($arrmedal as $medali=>$medal){
    echo " $medali = $medal<br><br>";
}

// reset($arrmedal);

// echo"<br>Menampilkan isi array Asosiatif dengan WHile dan LIST: <br>";
// while(list($medali,$medal) = each($arrmedal)){
//     echo"$medali = $medal<br>";
// }

// Struktur array
echo "----------------------------------------------------------------"."<br>"."<br>";

echo"Mencetak struktur array<br>";
$arrhewan=array("Harimau","Singa","Macan","Kuda");
$arrmedali=array("Akhyar"=>"emas","lukas"=>"silver","nicholas"=>"perak");
echo "<pre>";
print_r($arrhewan);
echo "<br>";
print_r($arrmedali);
echo"</pre>";

echo "----------------------------------------------------------------"."<br>"."<br>";

// asort dan arsort
echo"Mengurutkan array dengan asort dan arsort<br>";
$arrNilai = array("Fulan" => 80, "Fulin" => 90, "Fulun" => 75, "Falan" => 85);
echo "<b>Array sebelum diurutkan</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

sort($arrNilai);
reset($arrNilai);
echo "<b>Array setelah diurutkan dengan sort()</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

rsort($arrNilai);
reset($arrNilai);
echo "<b>Array setelah diurutkan dengan rsort()</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

echo "----------------------------------------------------------------"."<br>"."<br>";

// ksort dan krsort
echo"<h4>Mengurutkan array dengan ksort dan krsort<h4><br>";
$arrNilai = array("Fulan" => 80, "Fulin" => 90, "Fulun" => 75, "Falan" => 85);
echo "<b>Array sebelum diurutkan</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

ksort($arrNilai);
reset($arrNilai);
echo "<b>Array setelah diurutkan dengan sort()</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

krsort($arrNilai);
reset($arrNilai);
echo "<b>Array setelah diurutkan dengan rsort()</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

echo "----------------------------------------------------------------"."<br>"."<br>";
// Mengatur posisi pointer dalam
echo"<h4>Mengatur posisi pointer dalam<h4><br>";

$transport = array('Lancer',"Corolla","vios","baleno");
echo "<pre>";
print_r($transport);
echo "</pre>";

$mode = current($transport);
echo $mode . "<br>";

$mode = next($transport);
echo $mode . "<br>";

$mode = current($transport);
echo $mode . "<br>";

$mode = prev($transport);
echo $mode . "<br>";

$mode = end($transport);
echo $mode . "<br>";

$mode = current($transport);
echo $mode . "<br>";

echo "----------------------------------------------------------------"."<br>"."<br>";
// MMencari elemen array
echo"<h4>Mencari elemen array<h4><br>";
$arrada=array("udang","gajah","hiu");
    if(in_array("udang",$arrada)){
        echo "ada udang didalam array tersebut";
    }else{
        echo"tidak ditemukan";
    }

    echo "<br><br>----------------------------------------------------------------"."<br>"."<br>";

    // Fungsi tanpa return
echo"<h4>fungsi tanpa return<h4><br>";
function cetak_genap(){
    for($i=0;$i<=100;$i++){
        if($i%2==0){
            echo "$i, ";
        }
    }
}

cetak_genap();

  echo "<br><br>----------------------------------------------------------------"."<br>";

    // Fungsi tanpa return
echo"<h4>fungsi tanpa return value tapi dengan parameter</h4><br>";
function cetak_gnap($awal,$akhir){
    for($i=$awal;$i<=$akhir;$i++){
        if($i%2==0){
            echo "$i, ";
        }
    }
}
$a=20;
$b=100;
cetak_gnap($a,$b);

  echo "<br><br>----------------------------------------------------------------"."<br>";


  // Fungsi dengan return value & parameter
echo"<h4>fungsi dengan return value & parameter<h4><br>";

function luaspersegi($sisi){
    return $sisi * $sisi;
}
$a=20;
echo"Luas persegi jika setiap sisinya adalah $a = ";
echo luaspersegi($a);

  echo "<br><br>----------------------------------------------------------------"."<br>";


 // Passing by Value
echo"<h4>Passing by Value</h4><br>";
function tambah_string1($str){
    $str = $str . ", 3142mdpl";
    return $str;
}

$string = "Merbabu";
echo "\$string = $string<br>";
echo tambah_string1($string) . "<br>";
echo "\$string = $string<br>";
  echo "<br><br>----------------------------------------------------------------"."<br>";


 // Passing by reference
echo"<h4>Passing by reference</h4><br>";

function tambah_string(&$str){
    $str = $str . ", 3142mdpl";
    return $str;
}


$string = "Merbabu";
echo "\$string = $string<br>";
echo tambah_string($string) . "<br>";
echo "\$string = $string<br>";

 echo "<br><br>----------------------------------------------------------------"."<br>";


 // Menampilkan udf
echo"<h4>Menampilkan udf</h4><br>";
$arr=get_defined_functions();
echo"<pre>";
print_r($arr);
echo"</pre>";


    echo "<h2>Program 17</h2>";

    function luasSegitiga($alas, $tinggi){
        return 0.5*$alas*$tinggi;
    }

    $arr = get_defined_functions();
    
    // cek keberadaan fungsi UDF
    foreach($arr['user'] as $fungsi){

        if($fungsi == 'cetakgenap'){
            echo "Fungsi cetakgenap ada di PHP versi ini";
            break;
        }else{
            echo "Fungsi cetakgenap tidak ada";
            break;
        }
    }

    // cek keberadaan fungsi bawaan
    if (function_exists('exif_read_data')){
        echo "<br><br>Fungsi exif_read_data ada di PHP versi ini";

    }else{
        echo "<br><br>Fungsi exif_reada_data tidak ada";

    }

?>

